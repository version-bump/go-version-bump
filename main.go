package main

import (
	"flag"
	"fmt"
	"os"

	"version-bump/presets"
	"version-bump/version_strategy"

	"github.com/gogs/git-module"
)

func main() {
	// 1. Determine current version based on VersionStrategy
	var cwd string
	var err error
	// Setup the flags
	flag.StringVar(&cwd, "cwd", "", "Fuck my whole ass")
	flag.Parse()
	if cwd == "" {
		cwd, err = os.Getwd()
		if err != nil {
			panic(err)
		}
	}

	repo, err := git.Open(cwd)
	if err != nil {
		panic(err)
	}

	versionStrategy := version_strategy.PackageJsonVersionStrategy{}
	currentVersion := versionStrategy.GetCurrentVersion(repo, cwd)
	fmt.Println("Current version", currentVersion)

	// 2. Gather current commits from previous version using VersionStrategy

	commits, err := repo.Log(fmt.Sprintf("%s..%s", currentVersion, "HEAD"))

	if err != nil {
		panic(err)
	}

	// 3. Create new Preset
	p := presets.AngularPreset{}
	// 4. Pass current version and commits between previous and current to calculate the new version
	newVersion := p.CalculateVersion(currentVersion, commits)

	// 5. Generate the commit message
	newCommitMsg := p.GenerateCommit(newVersion)

	// 6. Create the commit.

	// Add all
	repo.Add(git.AddOptions{
		All: true,
	})
	// Make commit
	err = repo.Commit(&git.Signature{
		Name: "Version Bump",
	}, newCommitMsg)
	if err != nil {
		panic(err)
	}
}
