# Version Bump - Golang

As I haven't really used Golang seriously, and much of my previous experience with this was through my former employer,
I have decided that the best way to learn golang and it's ecosystem are by recreating a tool I've already made.

As such, this will be a documented journey of me figuring out Go!