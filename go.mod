module gitlab.com/version-bump/go-version-bump

go 1.20

require github.com/gogs/git-module v1.8.1

require github.com/mcuadros/go-version v0.0.0-20190308113854-92cdf37c5b75 // indirect
