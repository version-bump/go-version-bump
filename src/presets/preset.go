package presets

import "github.com/gogs/git-module"

// Required methods on a preset
type PresetMethods interface {
	// Gets passed the current value and the commits for the user to generate a new
	// packaged version
	CalculateVersion(current string, commits []*git.Commit) string
	// Creates the commit message for the new version.
	GenerateCommit(newVersion string) string
}

// Preset structure
type Preset struct {
	PresetMethods
}
