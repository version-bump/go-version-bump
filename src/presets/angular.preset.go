package presets

import (
	"fmt"
	"strings"

	"github.com/Masterminds/semver"
	"github.com/gogs/git-module"
)

type AngularPreset struct {
}

func (AngularPreset) CalculateVersion(
	current string,
	commits []*git.Commit,
) string {
	var v semver.Version
	version, err := semver.NewVersion(current)
	if err != nil {
		panic(err)
	}

	v = *version

	bumpLevel := "patch"

	for _, commit := range commits {
		if strings.Contains(strings.ToLower(commit.Message), "feat:") {
			bumpLevel = "minor"
		} else if strings.Contains(strings.ToLower(commit.Message), "breaking!:") {
			bumpLevel = "major"
		}
	}

	switch bumpLevel {
	case "minor":
		v = version.IncMinor()
	case "major":
		v = version.IncMajor()
	default:
		v = version.IncPatch()
	}

	return v.String()
}

func (AngularPreset) GenerateCommit(currentVersion string) string {
	return fmt.Sprintf("chore(release): %s", currentVersion)
}
