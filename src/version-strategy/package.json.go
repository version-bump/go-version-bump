package version_strategy

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"

	"github.com/Masterminds/semver"
	"github.com/gogs/git-module"
)

type PackageJsonVersionStrategy struct{}

type PackageJson struct {
	Version string      `json:"version"`
	Other   interface{} `json:"-"`
}

func (*PackageJsonVersionStrategy) GetCurrentVersion(repo *git.Repository, cwd string) string {
	// 1. check if we have a package.json file, get version from that.
	packagePath := fmt.Sprintf("%s/%s", cwd, "package.json")
	if _, err := os.Stat(packagePath); err == nil {
		packageContents, err := os.ReadFile(packagePath)
		if err != nil {
			panic(err)
		}
		var jsonObj PackageJson
		json.Unmarshal(packageContents, &jsonObj)
		if jsonObj.Version != "" {
			return jsonObj.Version
		}
	} else {
		// here means we have to do the weird ass task of searching through tags
		tags, err := repo.Tags()
		if err != nil {
			panic(err)
		}
		if len(tags) == 0 {
			return "1.0.0"
		}

		sort.Slice(tags, func(i, j int) bool {
			versionA, err := semver.NewVersion(tags[i])
			if err != nil {
				panic(err)
			}
			versionB, err := semver.NewVersion(tags[j])
			if err != nil {
				panic(err)
			}
			return versionA.GreaterThan(versionB)
		})
		return tags[0]
	}
	// We should realistically never get to this point, but you never know.
	return "0.0.0"
}

// Writes a new version to the determining source
func (*PackageJsonVersionStrategy) WriteNewVersion(newVersion string, cwd string) error {
	fileName := fmt.Sprintf("%s/%s", cwd, "package.json")
	if _, err := os.Stat(fileName); err != nil {
		return err
	}
	// If we get down here, then everything is fine.

	content, err := os.ReadFile(fileName)

	if err != nil {
		return err
	}

	var jsonData map[string]interface{}

	json.Unmarshal(content, &jsonData)
	jsonData["version"] = newVersion
	if marsh, err := json.MarshalIndent(jsonData, "", "  "); err == nil {
		os.WriteFile(fileName, marsh, 0666)
	} else {
		panic(err)
	}

	return nil
}
