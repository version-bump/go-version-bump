package version_strategy

import "github.com/gogs/git-module"

type VersionStrategyMethods interface {
	GetCurrentVersion(repo *git.Repository, cwd string) string
	WriteNewVersion(newVersion string, cwd string) error
}

type VersionStrategy struct {
	VersionStrategyMethods
}
